#include "board.h"
#include <vector>
/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
	taken.set(3 + 8 * 3);
	taken.set(3 + 8 * 4);
	taken.set(4 + 8 * 3);
	taken.set(4 + 8 * 4);
	black.set(4 + 8 * 3);
	black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
	Board *newBoard = new Board();
	newBoard->black = black;
	newBoard->taken = taken;
	return newBoard;
}

bool Board::occupied(int x, int y) {
	return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
	return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
	taken.set(x + 8*y);
	black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
	return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
	return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			Move move(i, j);
			if (checkMove(&move, side)) return true;
		}
	}
	return false;
}

/*
 * Returns all possible moves for the given side.
 */
std::vector<Move*> Board::getMoves(Side side) {
	std::vector<Move*> moves;
	
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			Move move(i, j);
			if (checkMove(&move, side)) {
				Move* new_move = new Move(i,j);
				moves.push_back(new_move);
			}
		}
	}
	return moves;
}

void Board::setBoardBits(bitset<64> new_black, bitset<64> new_taken) {
	this->black = new_black;
	this->taken = new_taken;
}

/*
 * Returns all possible moves for the given side.
 */
int Board::getScore(bitset<64> black, bitset<64> taken) {
	std::vector<Move*> moves;
	
	return countWhite() - countBlack();
}

bitset<64> Board::getTaken() {
	return this->taken;
}
bitset<64> Board::getBlack() {
	return this->black;
}
/*
 * Returns a move for the given side.
 */
int Board::getMinimaxMoves(bitset<64> black_now, bitset<64> taken_now, Side side, int depth) {
	//bitset<64> black_now = black;
	//bitset<64> taken_now = taken;
	int alpha = -999;
	//Move* best_move = new Move(-1,-1);

	
	std::vector<Move*> moves = this->getMoves(side);
	if (!hasMoves(side) || depth <= 0) { // terminal node or final depth reached
		//score_move ret;
		//ret.move = NULL;
		//ret.score = this->getScore(black, taken);
		return this->getScore(black_now, taken_now);
	}
	Side other = (side == BLACK) ? WHITE : BLACK;
	
	// Iterate through possible moves
	for (int i = 0; i < int(moves.size()); i++) {
		//score_move best;
		//best.move = NULL;
		//best.score = score;
		this->doMove(moves[i], side); // do a move;
		alpha = max(alpha, -this->getMinimaxMoves( this->getBlack(), this->getTaken(), other, depth-1));
		this->setBoardBits(black_now, taken_now);
	}
	return alpha;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
	// Passing is only legal if you have no moves.
	if (m == NULL) return !hasMoves(side);

	int X = m->getX();
	int Y = m->getY();

	// Make sure the square hasn't already been taken.
	if (occupied(X, Y)) return false;

	Side other = (side == BLACK) ? WHITE : BLACK;
	for (int dx = -1; dx <= 1; dx++) {
		for (int dy = -1; dy <= 1; dy++) {
			if (dy == 0 && dx == 0) continue;

			// Is there a capture in that direction?
			int x = X + dx;
			int y = Y + dy;
			if (onBoard(x, y) && get(other, x, y)) {
				do {
					x += dx;
					y += dy;
				} while (onBoard(x, y) && get(other, x, y));

				if (onBoard(x, y) && get(side, x, y)) return true;
			}
		}
	}
	return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
	// A NULL move means pass.
	if (m == NULL) return;

	// Ignore if move is invalid.
	if (!checkMove(m, side)) return;

	int X = m->getX();
	int Y = m->getY();
	Side other = (side == BLACK) ? WHITE : BLACK;
	for (int dx = -1; dx <= 1; dx++) {
		for (int dy = -1; dy <= 1; dy++) {
			if (dy == 0 && dx == 0) continue;

			int x = X;
			int y = Y;
			do {
				x += dx;
				y += dy;
			} while (onBoard(x, y) && get(other, x, y));

			if (onBoard(x, y) && get(side, x, y)) {
				x = X;
				y = Y;
				x += dx;
				y += dy;
				while (onBoard(x, y) && get(other, x, y)) {
					set(side, x, y);
					x += dx;
					y += dy;
				}
			}
		}
	}
	set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
	return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
	return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
	return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
	taken.reset();
	black.reset();
	for (int i = 0; i < 64; i++) {
		if (data[i] == 'b') {
			taken.set(i);
			black.set(i);
		} if (data[i] == 'w') {
			taken.set(i);
		}
	}
}
