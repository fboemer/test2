#include "player.h"

// Comment for testing git

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

	board = new Board();
	color = side;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {

	// Do opponent's move
	//opponentsMove->getX() <<opponentsMove->getY()
	std::cerr << "opponent's move" <<  std::endl;
	Side other = (color == BLACK) ? WHITE : BLACK;
	board->doMove(opponentsMove, Side(other));

	if (!board->hasMoves(color))
		return NULL;

	bitset<64> black_now = board->getBlack();
    bitset<64> taken_now = board->getTaken();

    std::vector<Move*> myMoves = board->getMoves(color);
    int best_ind = 0;
    int best_score = -999;
    for (int i = 0; i < int(myMoves.size()); i++)
    {
		int score = board->getMinimaxMoves(black_now, taken_now, color, 4);
		if (score > best_score) {
			best_score = score;
			best_ind = i;
		}
	}
    std::cerr << "my move" << myMoves[0]->getX() << myMoves[0]->getY() << std::endl;

	// restore board position
	board->setBoardBits(black_now, taken_now);
	
    
    board->doMove(myMoves[best_ind], Side(color));
	
	return myMoves[best_ind];
}
